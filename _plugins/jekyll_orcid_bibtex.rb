require 'json'
require 'open-uri'

module Jekyll

  class BibliographyFile < StaticFile
    def initialize(site, base, dir, orcid)
      @site = site
      @base = base
      @dir = dir
      @orcid = orcid

      if site.config['orcid_number']
        url = "https://pub.orcid.org/v3.0/#{orcid}/works"
        response = JSON.load(URI.open(url, 'Accept' => 'application/json'))
      end

      put_codes = response['group'].collect{ |entry| entry['work-summary'] }.flatten.collect{ |entry| entry['put-code'] }

      bibtex_entries = put_codes.map do |put_code|
        work_url = "https://pub.orcid.org/v3.0/#{orcid}/work/#{put_code}";
        response = JSON.load(URI.open(work_url, 'Accept' => 'application/json'));
        response['citation']['citation-value'] if defined? response['citation']['citation-value']
      end#.compact! removido ou não

      File.open(
        File.expand_path(site.source+"/"+dir+"/"+orcid+".bib"),
        File::WRONLY|File::CREAT) { |file| file.write(bibtex_entries.join("\n")) }

      super(site, base, dir, orcid)
    end
  end

  class BibliographyGenerator < Generator
    safe true
    priority :highest

    def generate(site)
      if site.config['orcid_number'] and site.config['orcid_updateData']
        puts "#################################################################################"
        puts "Downloading and parsing the OrcID data ..."
        puts "You may start/stop updating the .bib by setting 'orcid_updateData' to true/false"
        puts "If it fails now, you must go to OrcID and set the BIBTEX in all references"
        puts " "
        dir = site.config['scholar'] ? site.config['scholar']['source'] : "./_bibliography"
        file = BibliographyFile.new(site, site.source, dir, "#{site.config['orcid_number']}")
        puts "Done !"
        puts "#################################################################################"
      end
    end
  end

end
