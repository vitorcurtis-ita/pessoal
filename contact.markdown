---
layout: page
title: Contacts
---

<br/>

<aside><span class="icon icon--phone">{% include _icons/phone.svg %}</span> <a href="tel:+55-12-3947-5988">+55 (12) 3947-5988</a></aside>

<aside><span class="icon icon--email">{% include _icons/email.svg %}</span> <a href="mailto:curtis@ita.br">curtis@ita.br</a></aside>

## Mailing Address

    Technological Institute of Aeronautics (ITA)
    Computer Science Division (IEC)
    Praça Marechal Eduardo Gomes, 50 · 12228-900
    São José dos Campos/SP · Brazil

## Location

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14667.75057380533!2d-45.872237!3d-23.208943000000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf62c90e67688ab19!2sITA%20Computer%20Science%20Division!5e0!3m2!1sen!2sbr!4v1603506469068!5m2!1sen!2sbr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
