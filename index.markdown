---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: about
title: About me
---

<br/>

I received my MSc (2013) and PhD (2018) in *Electronic Engineering and Computer Science* from the *Aeronautics Institute of Technology* (ITA), São José dos Campos, Brazil. In 2017, I was a *Visiting Scholar* in the *Viterbi School of Engineering* at *The University of Southern California* (USC), Los Angeles, USA.

Since 2018, I have been an Assistant Professor with the *Computer Science Division*, *Department of Computer Systems*, ITA.

**2018-now** - *Assistant Professor* <br/>
&nbsp;&nbsp; Computer Science Division (IEC) <br/>
&nbsp;&nbsp; Aeronautics Institute of Technology (ITA)

**2017** - *Visiting Scholar* <br/>
&nbsp;&nbsp; University of Southern California (USC) <br/>
&nbsp;&nbsp; Advisor: [Viktor K. Prasanna](https://sites.usc.edu/prasanna)

**2014-2018** - *PhD in Electronic Engineering and Computer Science* <br/>
&nbsp;&nbsp; Aeronautics Institute of Technology (ITA) <br/>
&nbsp;&nbsp; Advisor: [Carlos A. A. Sanches](http://www.ita.br/~alonso)

**2011-2013** - *MSc in Electronic Engineering and Computer Science* <br/>
&nbsp;&nbsp; Aeronautics Institute of Technology (ITA) <br/>
&nbsp;&nbsp; Advisor: [Carlos A. A. Sanches](http://www.ita.br/~alonso)

## Research Interest

<br/>
My research interests include:

- High-Performance Computing
- Parallel Computing
- Algorithms
- Optimization
- Data Science

We are looking for motivated students interested in applications related to unmanned aerial vehicles (UAV) and algorithmic trading.

If you have a research proposal on any of these applications, please contact me by email.

### UAV

Some current challenges we are facing:

- models for the airspace command and control (see: [ACCESS 20'](https://doi.org/10.1109/access.2020.3030612))
- integration of the airspace for manned and unmanned aircraft
- route optimization for unmanned aircraft fleets
- simulation of applications with UAVs

### Algorithmic trading

Some current challenges we are facing:

- Event detection
- Robust approaches
- Forecasting
- Portfolio optimization

### Algorithms and HPC

Some current challenges we are facing:

- New dynamic programming methods (see: [EJOR 19'](https://doi.org/10.1016/j.ejor.2018.11.055))
- Boolean functions
- Domain languages and methods for high performance computing (HPC)
