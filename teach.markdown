---
layout: page
title: Teaching
permalink: /teaching/
---

# Undergraduate classes

#### CES-27 - **Distributed Processing** (2018)

Go to [Google Classroom](classroom.google.com)

#### CES-10 - **Introduction to Computer Science** (2019)

Go to [Google Classroom](classroom.google.com)

#### CES-11 - **Algorithms and Data Structures** (2019-now)

Go to [Google Classroom](classroom.google.com)

#### CCI-22 - **Computational Mathematics** (2019-now)

Go to [Google Classroom](classroom.google.com)

# Graduate classes

#### CE 265 - **Parallel Computing** (2020-now)

Weekly Schedule: 3 - 0 - 0 - 3
Requirements: approval
Recommended: CES-25 or similar

Syllabus: *History of the architecture of parallel computers and supercomputers. Flynn's Taxonomy. Current architectures: vector cores, multiple processors per chip (homogeneous and heterogeneous), graphics processing units. Interconnection networks. Languages to express parallel algorithms. Automatic extraction of parallelism from sequential programs. Parallel performance metrics. Characteristics and models of parallel algorithms. Classical, numeric, and non-numerical parallel algorithms.*

Go to [Google Classroom](classroom.google.com)

#### CS 249 - **Drone Simulation and Applications** (2021-now)

Weekly Schedule: 3 - 0 - 0 - 3
Requirements: CES-11 or similar
Recommended: CES-33 and CES-35

Syllabus: *Operational and regulatory aspects in the use of Drones; Classes of use; Drone classification based on size and applications; Standards and certifications; Multidisciplinary simulation of drones and applications; Hardware and system architectures; Mobility models for unmanned aerial vehicles; Wireless and mobile network communication models for drones; control and autopilot systems; Path planning; Perception based on virtual data from radar and camera; Airspace control models.*

Go to [Google Classroom](classroom.google.com)
